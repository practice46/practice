package com.space.practice.pojo;

/**
 * User: xyj
 * Date: 2020-09-05
 * Time: 11:17 下午
 */
public class TreeNode {

    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int val) {
        this.val = val;
    }

}
