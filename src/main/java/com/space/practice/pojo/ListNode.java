package com.space.practice.pojo;

/**
 * 链表实体
 *
 * @author xyj
 * @date 2020/9/9
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
    }

}
