package com.space.practice.circle.dependency;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 循环依赖
 *
 * @author xyj
 * @date 2021/5/6
 */
public class CircleTest {

    private final static Map<String, Object> singletoObjects = new ConcurrentHashMap<>(256);

    public static void main(String[] args) throws Exception {
        System.out.println(getBean(B.class).getA());
        System.out.println(getBean(A.class).getB());
    }

    private static <T> T getBean(Class<T> beanClass) throws Exception {
        String beanName = beanClass.getSimpleName().toLowerCase();
        if (singletoObjects.containsKey(beanName)) {
            return (T) singletoObjects.get(beanName);
        }

        T t = beanClass.newInstance();
        singletoObjects.put(beanName, t);
        Field[] fields = t.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            Class<?> type = field.getType();
            String fieldBeanName = type.getSimpleName().toLowerCase();
            field.set(t, singletoObjects.containsKey(fieldBeanName) ? singletoObjects.get(fieldBeanName) : getBean(type));
            field.setAccessible(false);
        }
        return t;
    }

}

class A {
    private B b;

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }
}

class B {
    private A a;

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }
}