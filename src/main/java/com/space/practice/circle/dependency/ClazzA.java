package com.space.practice.circle.dependency;

/**
 * 循环依赖
 *
 * @author xyj
 * @date 2021/5/6
 */
public class ClazzA {

    private ClazzB b = new ClazzB();

}
