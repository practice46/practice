package com.space.practice.pattern.template;

/**
 * 游戏
 *
 * @author xyj
 * @date 2020/9/5
 */
public abstract class Game {

    /**
     * 初始化
     */
    protected abstract void init();

    /**
     * 开始
     */
    protected abstract void start();

    /**
     * 结束
     */
    protected abstract void end();

    protected final void play() {
        init();
        start();
        end();
    }

}
