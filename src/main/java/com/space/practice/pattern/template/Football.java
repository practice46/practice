package com.space.practice.pattern.template;

/**
 * 足球
 *
 * @author xyj
 * @date 2020/9/5
 */
public class Football extends Game {
    @Override
    protected void init() {
        System.out.println("初始化游戏----足球");
    }

    @Override
    protected void start() {
        System.out.println("开始游戏----足球");
    }

    @Override
    protected void end() {
        System.out.println("结束游戏----足球");
    }
}
