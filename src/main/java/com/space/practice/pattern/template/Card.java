package com.space.practice.pattern.template;

/**
 * 卡片
 *
 * @author xyj
 * @date 2020/9/5
 */
public class Card extends Game {
    @Override
    protected void init() {
        System.out.println("初始化游戏----棋牌");
    }

    @Override
    protected void start() {
        System.out.println("开始游戏----棋牌");
    }

    @Override
    protected void end() {
        System.out.println("结束游戏----棋牌");
    }
}
