package com.space.practice.pattern.proxy.cglib;

/**
 * cglib测试类
 *
 * @author xyj
 * @date 2020/9/10
 */
public class CglibProxyDemo {
    public static void main(String[] args) {
        CglibProxy cglibProxy = new CglibProxy(new UserService());
        UserService userService = (UserService) cglibProxy.getTargetProxy();
        String userName = userService.getUserName(123L);
        System.out.println("123的用户名是：" + userName);
    }
}
