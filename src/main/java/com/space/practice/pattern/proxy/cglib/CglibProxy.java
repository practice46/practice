package com.space.practice.pattern.proxy.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * cglib代理实现
 *
 * @author xyj
 * @date 2020/9/10
 */
public class CglibProxy implements MethodInterceptor {

    private Object object;

    public CglibProxy(Object object) {
        this.object = object;
    }

    /**
     * proxy：       代理对象，CGLib动态生成的代理类实例
     * method：      目标对象的方法，上文中实体类所调用的被代理的方法引用
     * args：        目标对象方法的参数列表，参数值列表
     * methodProxy： 代理对象的方法，生成的代理类对方法的代理引用
     */
    @Override
    public Object intercept(Object proxy, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("开始拦截==");
        // 反射调用目标类方法
        Object invoke = method.invoke(object, objects);
        System.out.println("结束拦截==");
        return invoke;
    }

    /**
     * 获取代理实例
     *
     * @return 代理实例
     */
    public Object getTargetProxy() {
        // Enhancer类是cglib中的一个字节码增强器，它可以方便的为你所要处理的类进行扩展
        Enhancer enhancer = new Enhancer();
        // 1.将目标对象所在的类作为Enhancer类的父类
        enhancer.setSuperclass(UserService.class);
        // 2.通过实现MethodInterceptor实现方法回调
        enhancer.setCallback(this);
        // 3. 创建代理实例
        return enhancer.create();
    }

}
