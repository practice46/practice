package com.space.practice.pattern.proxy.cglib;

/**
 * 目标类
 *
 * @author xyj
 * @date 2020/9/10
 */
public class UserService {
    public String getUserName(Long userId) {
        System.out.println("获得用户名");
        return "user" + userId;
    }
}
