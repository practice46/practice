package com.space.practice.pattern.strategy;

/**
 * 使用 Context 来查看当它改变策略 Strategy 时的行为变化。
 * <p>
 * <p>
 * 我们将创建一个定义活动的 Strategy 接口和实现了 Strategy 接口的实体策略类。Context 是一个使用了某种策略的类。
 * StrategyPatternDemo，我们的演示类使用 Context 和策略对象来演示 Context 在它所配置或使用的策略改变时的行为变化。
 *
 * @author xyj
 * @date 2020/9/5
 */
public class StrategyPatternDemo {

    public static void main(String[] args) {
        Context context1 = new Context(new AddStrategy());
        Context context2 = new Context(new MultiplyStrategy());

        context1.execute(3, 4);
        context2.execute(3, 4);
    }

}
