package com.space.practice.pattern.strategy;

/**
 * 相成策略
 *
 * @author xyj
 * @date 2020/9/5
 */
public class MultiplyStrategy implements Strategy {

    @Override
    public void execute(int num1, int num2) {
        System.out.println("两数相乘之积：" + (num1 * num2));
    }

}
