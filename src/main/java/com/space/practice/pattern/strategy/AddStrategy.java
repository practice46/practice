package com.space.practice.pattern.strategy;

/**
 * 相加的策略
 *
 * @author xyj
 * @date 2020/9/4
 */
public class AddStrategy implements Strategy {

    @Override
    public void execute(int num1, int num2) {
        System.out.println("两数相加之和：" + (num1 + num2));
    }

}
