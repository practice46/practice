package com.space.practice.pattern.strategy;

/**
 * 策略接口
 *
 * @author xyj
 * @date 2020/9/4
 */
public interface Strategy {

    void execute(int num1, int num2);

}
