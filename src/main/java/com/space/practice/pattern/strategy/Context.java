package com.space.practice.pattern.strategy;

/**
 * Context
 *
 * @author xyj
 * @date 2020/9/5
 */
public class Context {
    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public void execute(int num1, int num2) {
        strategy.execute(num1, num2);
    }
}
