package com.space.practice.pattern.chain;

/**
 * 抽象的记录器类
 *
 * @author xyj
 * @date 2020/9/7
 */
public abstract class AbstractLogger {

    public static int INFO = 1;
    public static int DEBUG = 2;
    public static int ERROR = 3;

    protected int level;

    //责任链中的下一个元素
    protected AbstractLogger next;

    public void setNext(AbstractLogger next) {
        this.next = next;
    }

    public void logMsg(int level, String message) {
        if (this.level <= level) {
            write(message);
        }
        if (next != null) {
            next.logMsg(level, message);
        }
    }

    protected abstract void write(String message);

}
