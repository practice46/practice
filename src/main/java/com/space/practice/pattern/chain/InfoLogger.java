package com.space.practice.pattern.chain;

/**
 * 扩展了抽象记录器类的实体类
 *
 * @author xyj
 * @date 2020/9/7
 */
public class InfoLogger extends AbstractLogger {

    public InfoLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("InfoLogger:" + message);
    }
}
