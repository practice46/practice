package com.space.practice.pattern.chain;

/**
 * 创建抽象类 AbstractLogger，带有详细的日志记录级别。
 * 然后我们创建三种类型的记录器，都扩展了 AbstractLogger。
 * 每个记录器消息的级别是否属于自己的级别，如果是则相应地打印出来，否则将不打印并把消息传给下一个记录器。
 * <p>
 * 创建不同类型的记录器。赋予它们不同的错误级别，并在每个记录器中设置下一个记录器。
 * 每个记录器中的下一个记录器代表的是链的一部分。
 *
 * @author xyj
 * @date 2020/9/7
 */
public class ChainPatternDemo {

    public static void main(String[] args) {
        AbstractLogger chainLoggers = getChainLoggers();

//        chainLoggers.logMsg(AbstractLogger.INFO, "这是一个info");
//        chainLoggers.logMsg(AbstractLogger.DEBUG, "这是一个debug");
        chainLoggers.logMsg(AbstractLogger.ERROR, "这是一个error");
    }

    private static AbstractLogger getChainLoggers() {
        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger infoLogger = new ErrorLogger(AbstractLogger.INFO);
        AbstractLogger debugLogger = new ErrorLogger(AbstractLogger.DEBUG);
        errorLogger.setNext(infoLogger);
        infoLogger.setNext(debugLogger);
        return errorLogger;
    }

}
