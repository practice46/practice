package com.space.practice.pattern.observer;

/**
 * Observer
 *
 * @author xyj
 * @date 2020/9/5
 */
public abstract class Observer {

    protected Subject subject;

    /**
     * 要观察的更新动作
     */
    public abstract void update();

}
