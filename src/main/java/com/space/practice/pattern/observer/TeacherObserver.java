package com.space.practice.pattern.observer;

/**
 * 老师--观察者
 *
 * @author xyj
 * @date 2020/9/5
 */
public class TeacherObserver extends Observer {

    public TeacherObserver(Subject subject) {
        this.subject = subject;
        subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("状态发生改变，老师---知道了，state：" + subject.getState());
    }

}
