package com.space.practice.pattern.observer;

/**
 * 学生-观察者
 *
 * @author xyj
 * @date 2020/9/5
 */
public class StudentObserver extends Observer {

    public StudentObserver(Subject subject) {
        this.subject = subject;
        subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("状态发生改变，学生---知道了，state：" + subject.getState());
    }

}
