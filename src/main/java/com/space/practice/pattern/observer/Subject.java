package com.space.practice.pattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Subject
 *
 * @author xyj
 * @date 2020/9/5
 */
public class Subject {

    private List<Observer> observers = new ArrayList<>();
    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public void notifyAllObservers() {
        observers.forEach(Observer::update);
    }

}
