package com.space.practice.pattern.singleton;

/**
 * User: xyj
 * Date: 2020-09-05
 * Time: 11:06 下午
 */
public class Singleton {

    private static volatile Singleton singleton;

    /**
     * 私有化构造方法，保证外界无法访问
     */
    private Singleton() {

    }

    public static Singleton getSingleton() {
        if (singleton == null) {
            synchronized (Singleton.class) {
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }

    public void showMsg() {
        System.out.println("双检锁单例～～～");
    }

}
