package com.space.practice.pattern.singleton;

/**
 * 双检锁单例
 * User: xyj
 * Date: 2020-09-05
 * Time: 11:11 下午
 */
public class SingletonPatternDemo {

    public static void main(String[] args) {
        Singleton.getSingleton().showMsg();
    }

}
