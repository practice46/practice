package com.space.practice.test;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 测试使用
 *
 * @author xyj
 * @date 2020/7/14
 */
public class PersonalTest {

    private static ListNode reverseListNode(ListNode head) {
        ListNode next = null;
        ListNode pre = null;
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    private static ListNode findKthListNode(ListNode head, int k) {
        if (null == head || k <= 0) {
            return null;
        }
        ListNode node1 = head;
        ListNode node2 = head;
        int count = 0;
        int index = k;
        while (node1 != null) {
            node1 = node1.next;
            count++;
            if (k < 1) {
                node2 = node2.next;
            }
            k--;
        }
        if (index > count) {
            return null;
        }
        return node2;
    }

    private static ListNode removeNthListNode(ListNode head, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode node1 = dummy;
        ListNode node2 = dummy;
        while (node1 != null) {
            node1 = node1.next;
            if (n < 1 && node1 != null) {
                node2 = node2.next;
            }
            n--;
        }
        node2.next = node2.next.next;
        return dummy.next;
    }


    public static void main(String[] args) {
        ListNode a = new ListNode(1);
        ListNode b = new ListNode(2);
        ListNode c = new ListNode(3);
        ListNode d = new ListNode(4);
        a.next = b;
        b.next = c;
        c.next = d;
//        ListNode listNode = reverseListNode(a);
//        while (listNode != null) {
//            System.out.println(listNode.next + "=====" + listNode.value);
//            listNode = listNode.next;
//        }
//        ListNode listNode = findKthListNode(a, 6);
//        System.out.println(listNode.next + "=====" + listNode.value);
//        ListNode listNode = removeNthListNode(a, 2);
//        while (listNode != null) {
//            System.out.println("value=====" + listNode.value);
//            listNode = listNode.next;
//        }
        HashMap hashMap = new HashMap();
        hashMap.put("q","q");
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        concurrentHashMap.put("q","q");
        AtomicInteger atomicInteger = new AtomicInteger(1);

    }
}

class ListNode {
    int value;
    ListNode next = null;

    public ListNode() {
    }

    public ListNode(int value) {
        this.value = value;
    }

}

class ThreadLocalDemo {

    private static final ThreadLocal<Integer> TL_INT = ThreadLocal.withInitial(() -> 6);
    private static final ThreadLocal<String> TL_STRING = ThreadLocal.withInitial(() -> "Hello, world");

    public static void main(String... args) {
        // 6
        System.out.println(TL_INT.get());
        TL_INT.set(TL_INT.get() + 1);
        // 7
        System.out.println(TL_INT.get());
        TL_INT.remove();
        // 会重新初始化该value，6
        System.out.println(TL_INT.get());
    }
}


class ThreadLocalTest {
    public static void main(String[] args){
        for(int i=0;i<2;i++){
            new Thread(new Runnable(){
                @Override
                public void run() {
                    int data = new Random().nextInt();
                    System.out.println(Thread.currentThread().getName()
                            +" has put data:"+data);
                    MyThreadScopeData.getThreadInstance().setName("name"+data);
                    MyThreadScopeData.getThreadInstance().setAge(data);
                    new A().get();
                    new B().get();
                }
            }).start();
        }

        // 分析ThreadLocal与WeakReference
        ThreadLocal local = new ThreadLocal();
        local.set("当前线程名称："+Thread.currentThread().getName());//将ThreadLocal作为key放入threadLocals.Entry中
        Thread t = Thread.currentThread();//注意断点看此时的threadLocals.Entry数组刚设置的referent是指向Local的，referent就是Entry中的key只是被WeakReference包装了一下
        local = null;//断开强引用，即断开local与referent的关联，但Entry中此时的referent还是指向Local的，为什么会这样，当引用传递设置为null时无法影响传递内的结果
        System.gc();//执行GC
        t = Thread.currentThread();//这时Entry中referent是null了，被GC掉了，因为Entry和key的关系是WeakReference，并且在没有其他强引用的情况下就被回收掉了
//如果这里不采用WeakReference，即使local=null，那么也不会回收Entry的key，因为Entry和key是强关联
//但是这里仅能做到回收key不能回收value，如果这个线程运行时间非常长，即使referent GC了，value持续不清空，就有内存溢出的风险
//彻底回收最好调用remove
//即：local.remove();//remove相当于把ThreadLocalMap里的这个元素干掉了，并没有把自己干掉
        System.out.println(local);

    }
    static class A {
        public void get(){
            MyThreadScopeData mydate = MyThreadScopeData.getThreadInstance();
            System.out.println("A from "+Thread.currentThread().getName()
                    +"get data:"+mydate.getName()+" "+mydate.getAge());
        }
    }
    static class B {
        public void get(){
            MyThreadScopeData mydate = MyThreadScopeData.getThreadInstance();
            System.out.println("B from "+Thread.currentThread().getName()
                    +"get data:"+mydate.getName()+" "+mydate.getAge());
        }
    }
}

class MyThreadScopeData {
    private MyThreadScopeData() {
    }

    private static ThreadLocal<MyThreadScopeData> map = new ThreadLocal<MyThreadScopeData>();

    public static MyThreadScopeData getThreadInstance() {
        MyThreadScopeData instance = map.get();
        if (instance == null) {
            instance = new MyThreadScopeData();
            map.set(instance);
        }
        return instance;
//        return new MyThreadScopeData();
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private int age;
}