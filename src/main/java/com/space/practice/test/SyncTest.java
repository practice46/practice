package com.space.practice.test;

/**
 * SyncTest
 *
 * @author xyj
 * @date 2020/7/24
 */
public class SyncTest implements Runnable {
    private int count = 0;

    @Override
    public void run() {
        String name = Thread.currentThread().getName();
//        if ("add".equals(name)) {
//            addCount();
//        } else {
//            this.decCount();
//        }
        System.out.println(name);
    }

    public static void main(String[] args) throws InterruptedException {
        SyncTest syncTest = new SyncTest();
        Thread t1 = new Thread(syncTest, "1");
        Thread t2 = new Thread(syncTest, "2");
        Thread t3 = new Thread(syncTest, "3");
        Thread t4 = new Thread(syncTest, "4");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }

    public void decCount() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                try {
                    count = count - 1;
                    System.out.println(Thread.currentThread().getName() + "[" + i + "]-----------" + count);
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void addCount() {
//        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                try {
                    count = count + 1;
                    System.out.println(Thread.currentThread().getName() + "[" + i + "]-----------" + count);
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
//        }
    }

}
