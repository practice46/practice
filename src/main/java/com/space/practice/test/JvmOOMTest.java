package com.space.practice.test;

import java.util.ArrayList;
import java.util.List;

/**
 * JvmOOMTest
 *
 * @author xyj
 * @date 2020/7/30
 */
public class JvmOOMTest {

//    private int stackLength = 0;
//
//    public void stackOverflow() {
//        ++stackLength;
//        stackOverflow();
//    }
//
//    public static void main(String[] args) {
//        JvmOOMTest test = new JvmOOMTest();
//        try {
//            test.stackOverflow();
//        } catch (Throwable e) {
//            System.out.println("stack length: " + test.stackLength);
//            throw e;
//        }
//    }

//
    private static class HeapOomObject {
    }

    private static class User {
        int age;
        String name;

        public User() {
        }

        public User(int age, String name) {
            this.age = age;
            this.name = name;
        }
    }

    public static void main(String[] args) {
        List<HeapOomObject> list = new ArrayList<HeapOomObject>();
        int a = 1;
        List<User> userList = new ArrayList<>();
        List<String> stringList = new ArrayList<>();
        while (true) {
            list.add(new HeapOomObject());
//            userList.add(new User(a++, "name----" + a));
//            stringList.add(String.valueOf(a++).intern());
        }
    }


//    static class MethodAreaOomObject {
//    }
//    public static void main(String[] args) {
//        while(true){
//            Enhancer enhancer = new Enhancer();
//            enhancer.setSuperclass(MethodAreaOomObject.class);
//            enhancer.setUseCache(false);
//            enhancer.setCallback(new MethodInterceptor() {
//                @Override
//                public Object intercept(Object obj, Method method, Object[] args,
//                                        MethodProxy proxy) throws Throwable {
//                    return proxy.invoke(obj, args);
//                }
//            });
//            enhancer.create();
//        }
//    }

}


