package com.space.practice.test;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

/**
 * StackTest
 *
 * @author xyj
 * @date 2020/8/21
 */
public class StackTest {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(111);
        stack.push(222);
        Integer pop = stack.pop();
        Integer peek = stack.peek();
        System.out.println(peek);
        System.out.println(pop);
        boolean empty = stack.empty();
        boolean empty1 = stack.isEmpty();


        Deque<Integer> deque = new LinkedList<>();
        deque.offer(1);
        deque.offer(2);
        deque.push(3);
        Integer poll = deque.poll();
        System.out.println(poll);
        Integer peek1 = deque.peek();
        System.out.println(peek1);

    }

}
