package com.space.practice.test;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * ProviderConsumerTest
 *
 * @author xyj
 * @date 2020/8/5
 */
public class ProviderConsumerTest {
    public static final int CAPACITY = 5;

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition fullCondition = lock.newCondition();
        //队列空的条件
        Condition emptyCondition = lock.newCondition();

        Queue<Integer> queue = new LinkedList<>();
        Thread producer1 = new Producer(queue, CAPACITY, "p1", lock, emptyCondition, fullCondition);
        Thread producer2 = new Producer(queue, CAPACITY, "p2", lock, emptyCondition, fullCondition);
        Thread consumer1 = new Consumer(queue, CAPACITY, "c1", lock, emptyCondition, fullCondition);
        Thread consumer2 = new Consumer(queue, CAPACITY, "c2", lock, emptyCondition, fullCondition);
        Thread consumer3 = new Consumer(queue, CAPACITY, "c3", lock, emptyCondition, fullCondition);

        producer1.start();
        producer2.start();
        consumer1.start();
        consumer2.start();
        consumer3.start();
    }

}

class Producer extends Thread {
    private Queue<Integer> queue;
    private int capacity;
    private String name;
    int i = 0;
    Lock lock;
    Condition emptyCondition;
    //队列空的条件
    Condition fullCondition;

    public Producer(Queue<Integer> queue, int capacity, String name, Lock lock, Condition emptyCondition, Condition fullCondition) {
        this.queue = queue;
        this.capacity = capacity;
        this.name = name;
        this.lock = lock;
        this.emptyCondition = emptyCondition;
        this.fullCondition = fullCondition;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (true) {
            lock.lock();
            try {
                while (queue.size() == capacity) {
                    System.out.println("生产者满了" + name);
                    try {
                        fullCondition.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                queue.offer(i++);
                emptyCondition.signalAll();
            } finally {
                lock.unlock();
            }

        }
    }
}

class Consumer extends Thread {
    private Queue<Integer> queue;
    private int capacity;
    private String name;
    int i = 0;
    Lock lock;
    Condition emptyCondition;
    //队列空的条件
    Condition fullCondition;

    public Consumer(Queue<Integer> queue, int capacity, String name, Lock lock, Condition emptyCondition, Condition fullCondition) {
        this.queue = queue;
        this.capacity = capacity;
        this.name = name;
        this.lock = lock;
        this.emptyCondition = emptyCondition;
        this.fullCondition = fullCondition;
    }

    @Override
    public void run() {

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (true) {
            lock.lock();
            try {
                while (queue.isEmpty()) {
                    System.out.println("消费没了" + name);
                    try {
                        emptyCondition.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                queue.poll();
                fullCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }
    }
}