package com.space.practice.test;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * DubboTest
 *
 * @author xyj
 * @date 2020/8/5
 */
public class DubboTest {

    public static void main(String[] args) {
        ServiceLoader<IDubboService> load = ServiceLoader.load(IDubboService.class);
        Iterator<IDubboService> iterator = load.iterator();
        while (iterator.hasNext()) {
            IDubboService next = iterator.next();
            next.sayDubbo();
        }
    }

}

interface IDubboService {
    void sayDubbo();
}

class DubboService implements IDubboService {

    @Override
    public void sayDubbo() {
        System.out.println("------------------");
    }
}