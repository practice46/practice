package com.space.practice.test;


import java.util.HashSet;
import java.util.Set;

/**
 * Singleton
 *
 * @author xyj
 * @date 2020/8/4
 */
public class Singleton {

    private static Singleton singleton;
//    private static volatile Singleton singleton;

    public static Singleton getSingleton() {
        if (singleton == null) {
            synchronized (Singleton.class) {
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }


    public static void main(String[] args) throws Exception {
        Set<Singleton> set = new HashSet<Singleton>();
        int i = 0;
        while (i < 10000) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    set.add(Singleton.getSingleton());
                }
            }).start();
            i++;
        }
        System.out.println(set.size());
    }

}
