package com.space.practice.test;

/**
 * DieLockTest
 *
 * @author xyj
 * @date 2020/7/30
 */
public class DeadLockTest {

    private  Object a = new Object();
    private static Object b = new Object();

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (new DeadLockTest().a) {
                    System.out.println("1-----------");
                    try {
                        Thread.sleep(5);
                        synchronized (b) {
                            System.out.println("2-----------");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (b) {
                    System.out.println("3-----------");
                    try {
                        Thread.sleep(5);
                        synchronized (new DeadLockTest().a) {
                            System.out.println("4-----------");
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        System.out.println("5-----------");
    }

}
