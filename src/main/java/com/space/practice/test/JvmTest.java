package com.space.practice.test;

/**
 * JvmTest
 *
 * @author xyj
 * @date 2020/7/30
 */
public class JvmTest {

    public int compute() {
        int a = 1;
        int b = 2;
        int c = (a + b) * 10;
        return c;
    }

    public static void main(String[] args) {
        JvmTest jvmTest = new JvmTest();
        int result = jvmTest.compute();
        System.out.println(result);
        System.out.println("----the---end----");
    }

}