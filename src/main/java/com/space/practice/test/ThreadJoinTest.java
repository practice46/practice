package com.space.practice.test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * ThreadJoinTest
 *
 * @author xyj
 * @date 2020/8/4
 */
public class ThreadJoinTest {

    public static void main(String[] args) {
//        Thread thread1 = new Thread(() -> {
//            System.out.println("-------1---------");
//        });
//        Thread thread2 = new Thread(() -> {
//            System.out.println("-------2---------");
//        });
//        Thread thread3 = new Thread(() -> {
//            System.out.println("-------3---------");
//        });
//        thread1.start();
//        thread2.start();
//        thread3.start();

        ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 1L, TimeUnit.MINUTES, new ArrayBlockingQueue<>(1));
        final int COUNT_BITS = Integer.SIZE - 3;
        final int CAPACITY = (1 << COUNT_BITS) - 1;
        final int RUNNING = -1 << COUNT_BITS;
        final int SHUTDOWN = 0 << COUNT_BITS;
        final int STOP = 1 << COUNT_BITS;
        final int TIDYING = 2 << COUNT_BITS;
        final int TERMINATED = 3 << COUNT_BITS;
        /**
         * -------COUNT_BITS---------29
         * -------CAPACITY---------536870911
         * -------RUNNING----------536870912
         * -------SHUTDOWN---------0
         * -------STOP---------536870912
         * -------TIDYING---------1073741824
         * -------TERMINATED---------1610612736
         */
        System.out.println("-------COUNT_BITS---------" + COUNT_BITS);
        System.out.println("-------CAPACITY---------" + CAPACITY);
        System.out.println("-------RUNNING---------" + RUNNING);
        System.out.println("-------SHUTDOWN---------" + SHUTDOWN);
        System.out.println("-------STOP---------" + STOP);
        System.out.println("-------TIDYING---------" + TIDYING);
        System.out.println("-------TERMINATED---------" + TERMINATED);


    }

}
