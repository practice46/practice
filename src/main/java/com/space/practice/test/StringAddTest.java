package com.space.practice.test;

/**
 * 给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和
 *
 * @author xyj
 * @date 2020/8/11
 */
public class StringAddTest {

    public static String addStrings(String num1, String num2) {
        int i = num1.length() - 1;
        int j = num2.length() - 1;
        int temp = 0;
        StringBuilder sb = new StringBuilder();
        while (i >= 0 || j >= 0 || temp != 0) {
            int x = i >= 0 ? num1.charAt(i) - '0' : 0;
            int y = j >= 0 ? num2.charAt(j) - '0' : 0;
            int position = x + y + temp;
            sb.append(position % 10);
            temp = position / 10;
            i--;
            j--;
        }
        StringBuilder reverse = sb.reverse();
        return reverse.toString();
    }

    public static void main(String[] args) {
        System.out.println(addStrings("111", "2233"));
    }

}
