package com.space.practice.juc.thread.synchronizers.countdownlatch;

import com.space.practice.juc.thread.synchronizers.ThreadCommonTest;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * CountDownLatchDemo
 *
 * @author xyj
 * @date 2020/9/14
 */
public class Demo implements Runnable {

    private int sleepTime;
    private CountDownLatch latch;

    public Demo(int sleepTime, CountDownLatch latch) {
        this.sleepTime = sleepTime;
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            ThreadCommonTest.print(" is Running ");
            TimeUnit.SECONDS.sleep(sleepTime);
            ThreadCommonTest.print(" is finished ");
            latch.countDown();
        } catch (InterruptedException e) {
            ThreadCommonTest.print(" InterruptedException ");
        }

    }
}
