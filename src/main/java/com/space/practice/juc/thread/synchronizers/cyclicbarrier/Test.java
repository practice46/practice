package com.space.practice.juc.thread.synchronizers.cyclicbarrier;

import com.space.practice.juc.thread.synchronizers.ThreadCommonTest;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * test
 *
 * @author xyj
 * @date 2020/9/14
 */
public class Test {

    public static void main(String[] args) {
        int count = 5;

        CyclicBarrier cyb = new CyclicBarrier(count, () -> ThreadCommonTest.print(" 所有线程到达栅栏处，可以做一些统一处理 "));

        ExecutorService es = Executors.newFixedThreadPool(count);
        for (int i = 0; i < count; i++) {
            es.execute(new Demo(i + 1, cyb));
        }

    }

}
