package com.space.practice.juc.thread.pool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池demo
 *
 * @author xyj
 * @date 2020/9/7
 */
public class ThreadPoolDemo {

    public static void main(String[] args) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(2, 5, 1, TimeUnit.SECONDS, new ArrayBlockingQueue<>(3));
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            try {
                threadPoolExecutor.execute(() -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "线程启动了~~" + (finalI + 1));
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int poolSize = threadPoolExecutor.getPoolSize();
        System.out.println(Thread.currentThread().getName() + "===poolSize===" + poolSize);
    }


}
