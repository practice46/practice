package com.space.practice.juc.thread.synchronizers;

import java.time.LocalTime;

/**
 * 公共使用
 *
 * @author xyj
 * @date 2020/9/14
 */
public class ThreadCommonTest {

    public static void print(String desc) {
        System.out.println(LocalTime.now() + " " + Thread.currentThread().getName() + desc);
    }

}
