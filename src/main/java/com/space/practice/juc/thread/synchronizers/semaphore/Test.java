package com.space.practice.juc.thread.synchronizers.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * test
 *
 * @author xyj
 * @date 2020/9/14
 */
public class Test {

    public static void main(String[] args) {
        int threadCount = 1000;
        Semaphore semaphore = new Semaphore(3);
        ExecutorService es = Executors.newFixedThreadPool(threadCount);
        for (int i = 0; i < threadCount; i++) {
            es.execute(new Demo( 1, semaphore));
        }

    }

}
