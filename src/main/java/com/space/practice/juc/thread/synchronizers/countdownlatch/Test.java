package com.space.practice.juc.thread.synchronizers.countdownlatch;

import com.space.practice.juc.thread.synchronizers.ThreadCommonTest;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 测试
 *
 * @author xyj
 * @date 2020/9/14
 */
public class Test {

    public static void main(String[] args) {
        int count = 5;
        CountDownLatch latch = new CountDownLatch(count);
        ExecutorService es = Executors.newFixedThreadPool(count);
        for (int i = 0; i < count; i++) {
            es.execute(new Demo(count + 1, latch));
        }

        try {
            ThreadCommonTest.print(" waiting ");
            latch.await();
            ThreadCommonTest.print(" continue ");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            es.shutdown();
        }

    }

}
