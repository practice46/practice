package com.space.practice.juc.thread.synchronizers.semaphore;

import com.space.practice.juc.thread.synchronizers.ThreadCommonTest;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * demo
 *
 * @author xyj
 * @date 2020/9/14
 */
public class Demo implements Runnable {

    private int sleepTime;
    private Semaphore semaphore;

    public Demo(int sleepTime, Semaphore semaphore) {
        this.sleepTime = sleepTime;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            // 获取资源
            semaphore.acquire();
            int queueLength = semaphore.getQueueLength();
            ThreadCommonTest.print(" 占用一个资源 ,queueLength:" + queueLength);
            TimeUnit.SECONDS.sleep(sleepTime);
            ThreadCommonTest.print(" 资源使用结束，释放资源 ");
            // 释放资源
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
