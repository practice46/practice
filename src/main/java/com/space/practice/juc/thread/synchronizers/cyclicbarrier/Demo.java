package com.space.practice.juc.thread.synchronizers.cyclicbarrier;

import com.space.practice.juc.thread.synchronizers.ThreadCommonTest;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * demo
 *
 * @author xyj
 * @date 2020/9/14
 */
public class Demo implements Runnable {

    private int sleepTime;
    private CyclicBarrier cyclicBarrier;

    public Demo(int sleepTime, CyclicBarrier cyclicBarrier) {
        this.sleepTime = sleepTime;
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        ThreadCommonTest.print(" 正在running ");
        try {
            TimeUnit.SECONDS.sleep(sleepTime);
            ThreadCommonTest.print(" 到达栅栏处，等待其他线程到达 ");
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        ThreadCommonTest.print(" 所有线程到达栅栏处，继续执行各自任务 ");

    }
}
