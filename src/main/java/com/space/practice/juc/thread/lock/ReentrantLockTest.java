package com.space.practice.juc.thread.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * AQS框架的应用实现
 *
 * @author xyj
 * @date 2021/3/12
 */
public class ReentrantLockTest {

    public static void main(String[] args) {

        ReentrantLock reentrantLock = null;
        try {
            reentrantLock = new ReentrantLock(false);
            reentrantLock.lock();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            reentrantLock.unlock();
        }

    }

}
