package com.space.practice.leetcode.tree;

import com.space.practice.pojo.TreeNode;

import java.util.Stack;

/**
 * 合法二叉搜索树
 * 实现一个函数，检查一棵二叉树是否为二叉搜索树。
 * 示例 1:
 * 输入:
 *     2
 *    / \
 *   1   3
 * 输出: true
 * 示例 2:
 * 输入:
 *     5
 *    / \
 *   1   4
 *      / \
 *     3   6
 * 输出: false
 * 解释: 输入为: [5,1,4,null,null,3,6]。
 *      根节点的值为 5 ，但是其右子节点值为 4 。
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/legal-binary-search-tree-lcci
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @author xyj
 * @date 2020/9/11
 */
public class IsValidBST {

    public static void main(String[] args) {
        TreeNode node = new TreeNode(5);
        node.left = new TreeNode(1);
        node.right = new TreeNode(4);
        node.right.left = new TreeNode(3);
        node.right.right = new TreeNode(6);
        boolean res = traversal(node);
        System.out.println(res);
    }

    public static boolean traversal(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        Integer value = null;
        TreeNode curr = root;
        while (curr != null || !stack.isEmpty()) {
            while (curr != null) {
                stack.push(curr);
                curr = curr.left;
            }
            curr = stack.pop();
            if (value != null && curr.val <= value) {
                return false;
            }
            value = curr.val;
            curr = curr.right;
        }
        return true;
    }

}
