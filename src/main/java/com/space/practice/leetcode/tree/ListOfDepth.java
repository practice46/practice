package com.space.practice.leetcode.tree;

import com.space.practice.pojo.ListNode;
import com.space.practice.pojo.TreeNode;

import java.util.*;

/**
 * 特定深度节点链表
 * 给定一棵二叉树，设计一个算法，创建含有某一深度上所有节点的链表（比如，若一棵树的深度为 D，则会创建出 D 个链表）。返回一个包含所有深度的链表的数组。
 * 示例：
 * 输入：[1,2,3,4,5,null,7,8]
 *
 *         1
 *        /  \
 *       2    3
 *      / \    \
 *     4   5    7
 *    /
 *   8
 *
 * 输出：[[1],[2,3],[4,5,7],[8]]
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/list-of-depth-lcci
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @author xyj
 * @date 2020/9/9
 */
public class ListOfDepth {

    public static void main(String[] args) {
        TreeNode node = new TreeNode(1);
        node.left = new TreeNode(2);
        node.right = new TreeNode(3);
        node.right.right = new TreeNode(7);
        node.left.left = new TreeNode(4);
        node.left.left.left = new TreeNode(8);
        node.left.right = new TreeNode(5);
        ListNode[] res = traversal(node);
        System.out.println(Arrays.toString(res));
    }

    public static ListNode[] traversal(TreeNode tree) {
        List<ListNode> res = new ArrayList<>();
        if (tree == null) {
            return res.toArray(new ListNode[0]);
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(tree);
        ListNode dummy = new ListNode(0);
        while (!queue.isEmpty()) {
            int size = queue.size();
            ListNode temp = dummy;
            for (int i = 0; i < size; i++) {
                TreeNode curr = queue.poll();
                temp.next = new ListNode(curr.val);
                temp = temp.next;
                if (curr.left != null) {
                    queue.add(curr.left);
                }
                if (curr.right != null) {
                    queue.add(curr.right);
                }
            }
            res.add(dummy.next);
            dummy.next = null;
        }
        return res.toArray(new ListNode[0]);
    }

}
