package com.space.practice.leetcode.tree;

import com.space.practice.pojo.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定一个二叉树和一个目标和，判断该树中是否存在根节点到叶子节点的路径，这条路径上所有节点值相加等于目标和。
 *
 * 说明: 叶子节点是指没有子节点的节点。
 *
 * 示例: 
 * 给定如下二叉树，以及目标和 sum = 22，
 *
 *               5
 *              / \
 *             4   8
 *            /   / \
 *           11  13  4
 *          /  \      \
 *         7    2      1
 * 返回 true, 因为存在目标和为 22 的根节点到叶子节点的路径 5->4->11->2。
 *
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/data-structure-binary-tree/xo566j/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 *
 * @author xyj
 * @date 2020/9/7
 */
public class HasPathSum {

    public static void main(String[] args) {
        TreeNode node = new TreeNode(5);
        node.left = new TreeNode(4);
        node.left.left = new TreeNode(11);
        node.left.left.left = new TreeNode(7);
        node.left.left.right = new TreeNode(2);
        node.right = new TreeNode(8);
        node.right.left = new TreeNode(13);
        node.right.right = new TreeNode(4);
        node.right.right.right = new TreeNode(1);
        System.out.println(recursion(node, 22));
        System.out.println(traversal(node, 22));
    }

    public static boolean recursion(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        if (root.left == null && root.right == null) {
            return sum == root.val;
        }
        return recursion(root.left, sum - root.val) || recursion(root.right, sum - root.val);
    }

    public static boolean traversal(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        Queue<Integer> valQueue = new LinkedList<>();
        nodeQueue.offer(root);
        valQueue.offer(root.val);
        while (!nodeQueue.isEmpty()) {
            TreeNode currNode = nodeQueue.poll();
            Integer currVal = valQueue.poll();
            if (currNode.left == null && currNode.right == null) {
                if (currVal == sum) {
                    return true;
                }
            }
            if (currNode.left != null) {
                nodeQueue.add(currNode.left);
                valQueue.add(currVal + currNode.left.val);
            }
            if (currNode.right != null) {
                nodeQueue.add(currNode.right);
                valQueue.add(currVal + currNode.right.val);
            }
        }
        return false;
    }


}
