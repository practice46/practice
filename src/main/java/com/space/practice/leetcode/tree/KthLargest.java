package com.space.practice.leetcode.tree;

import com.space.practice.pojo.TreeNode;

import java.util.*;

/**
 * 二叉搜索树的第k大节点
 * 给定一棵二叉搜索树，请找出其中第k大的节点。
 * 示例 1:
 * 输入: root = [3,1,4,null,2], k = 1
 *    3
 *   / \
 *  1   4
 *   \
 *    2
 * 输出: 4
 * 示例 2:
 * 输入: root = [5,3,6,2,4,null,null,1], k = 3
 *        5
 *       / \
 *      3   6
 *     / \
 *    2   4
 *   /
 *  1
 * 输出: 4
 * 限制：
 * 1 ≤ k ≤ 二叉搜索树元素个数
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/er-cha-sou-suo-shu-de-di-kda-jie-dian-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @author xyj
 * @date 2020/9/9
 */
public class KthLargest {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(3);
        node.left = new TreeNode(1);
        node.right = new TreeNode(4);
        node.left.right = new TreeNode(2);
        int res = recursion(node, 1);
        System.out.println(res);
    }

    public static int recursion(TreeNode root, int k) {
        if (root == null) {
            return 0;
        }
        List<Integer> list = new ArrayList<>();
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        nodeQueue.add(root);
        while (!nodeQueue.isEmpty()) {
            TreeNode curr = nodeQueue.poll();
            list.add(curr.val);
            if (curr.left != null) {
                nodeQueue.add(curr.left);
            }
            if (curr.right != null) {
                nodeQueue.add(curr.right);
            }
        }
        Queue<Integer> queue = new PriorityQueue<>(k);
        for (Integer item : list) {
            if (queue.size() < k) {
                queue.offer(item);
            } else if (queue.peek() < item) {
                queue.poll();
                queue.offer(item);
            }
        }
        return queue.poll();
    }

}
