package com.space.practice.reflect;

import com.space.practice.PracticeApplication;

import java.lang.reflect.Method;

/**
 * Demo
 *
 * @author xyj
 * @date 2020/9/28
 */
public class Demo {

    public static void main(String[] args) throws ClassNotFoundException {
//        Class<?> practiceApplication = Class.forName("practiceApplication");
//        Method[] methods = practiceApplication.getMethods();
//        System.out.println(methods);

        Class<PracticeApplication> practiceApplicationClass = PracticeApplication.class;
        Method[] methods1 = practiceApplicationClass.getMethods();
        for (Method method : methods1) {
            System.out.println(method);
        }

        Class<? extends PracticeApplication> aClass = new PracticeApplication().getClass();
        Method[] methods2 = aClass.getDeclaredMethods();
        for (Method method : methods2) {
            System.out.println(method);
        }

    }

}
